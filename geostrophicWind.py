# MTMW12 assignment 3. Ieuan Higgs 14 Oct 2020
# Python3 code to numerically differentiate the pressure in order to calculate
# the geostrophic wind relation using 2-point differencing and
# compare with the analytic solution and plot


# Need to change N, and see if it behaves quadratically


import numpy as mp
import matplotlib.pyplot as plt
from differentiate import *
from physProps import *


def geostrophicWind(N=10, display_graphs=False, flags=None, return_improved=True):
    """Calculate the geostrophic wind analytically and numerically and plot"""
    if flags is None:
        flags = []
    ymin = physProps["ymin"]
    ymax = physProps["ymax"]
    dy = (ymax - ymin) / N  # length of spacing

    # The spatial domain, y:
    y = np.linspace(ymin, ymax, N + 1)

    # The pressure at the u points and the exact geostrophic wind
    p = pressure(y, physProps)
    uExact = uGeoExact(y, physProps)

    # The pressure gradient and wind using two-point diffs
    dpdy = gradient_2point(p, dy)
    u_2point = geoWind(dpdy, physProps)

    # The pressure gradient and wind using two-point diffs and 3-point diffs at end points
    dpdy_imp = gradient_2point_improved(p, dy)
    u_2point_imp = geoWind(dpdy_imp, physProps)

    # The pressure gradient and wind using centered four-point diffs
    dpdy_4point = gradient_4point(p, dy)
    u_4point = geoWind(dpdy_4point, physProps)

    if display_graphs:
        # Graph to compare the numerical and analytical solution
        font = {'size': 14}
        plt.rc('font', **font)

        # Plot the approximate and exact win at y points
        plt.plot(y / 1000, uExact, 'k-', label='Exact', color='black', linewidth=2.0)
        if flags[0]: plt.plot(y / 1000, u_2point, 'k-', label='Two-Point', ms=12, linewidth=2.0, color='red')
        if flags[1]: plt.plot(y / 1000, u_2point_imp, 'k--', label='Two-point improved', ms=12, linewidth=2.0, color='blue')
        if flags[2]: plt.plot(y / 1000, u_4point, 'k-.', label='Four-Point', ms=7, linewidth=2.0, color='orange')
        plt.legend(loc='best', fontsize='small')
        plt.xlabel('y (km)')
        plt.ylabel('u (m/s)')
        plt.tight_layout()
        plt.savefig('geoWindCent_' + str(int(flags[0])) + str(int(flags[1])) + str(int(flags[2])) + '.png')
        plt.show()

        # plot the errors
        if flags[0]: plt.plot(y / 1000, abs(u_2point - uExact), 'k-', label='Two-point', ms=12, linewidth=2.0, color='red')
        if flags[1]: plt.plot(y / 1000, abs(u_2point_imp - uExact), 'k--', label='Two-point improved', ms=12, linewidth=2.0, color='blue')
        if flags[2]: plt.plot(y / 1000, abs(u_4point - uExact), 'k-.', label='Four-point', ms=12, linewidth=2.0, color='orange')

        plt.legend(loc='best', fontsize='small')
        plt.axhline(linestyle='-', color='k')
        plt.xlabel('y (km)')
        plt.ylabel('u error (m/s)')
        plt.tight_layout()
        plt.savefig('geoWindErrorsCent_' + str(int(flags[0])) + str(int(flags[1])) + str(int(flags[2])) + '.png')
        plt.show()

    if return_improved:
        return u_2point_imp - uExact
    return u_2point - uExact

# geostrophicWind(N=10, display_graphs=True, flags=[True, False, False])
# geostrophicWind(N=10, display_graphs=True, flags=[True, True, False])

