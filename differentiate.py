import numpy as np
import math
# Functions for calculating gradients


def gradient_2point(f, dx):
    """ The gradient of one dimensional array f assuming points are a distance
        dx apart using 2-point differences. Returns an array the same size as f"""
    # Initialised the array for the gradient to be the same size as f
    dfdx = np.zeros_like(f)
    # Two point differences at the end points
    dfdx[0] = (f[1] - f[0])/dx
    dfdx[-1] =(f[-1] - f[-2])/dx
    # Centred differences for the mid-points
    for i in range (1, len(f)-1):
        dfdx[i] = (f[i+1] - f[i-1])/(dx*2)
    return dfdx


def gradient_2point_improved(f, dx):
    """ The gradient of one dimensional array f assuming points are a distance
        dx apart using 2-point differences. Returns an array the same size as f"""
    # Initialised the array for the gradient to be the same size as f
    dfdx = np.zeros_like(f)
    # Two point differences at the end points - forward and backward differences are second order accuracy
    dfdx[0] = (-3*f[0] + 4*f[1] - f[2])/(dx*2)
    dfdx[-1] = (3*f[-1] - 4*f[-2] + f[-3])/(dx*2)
    # Centred differences for the mid-points
    for i in range (1 , len(f)-1):
        dfdx[i] = (f[i+1] - f[i-1])/(dx*2)
    return dfdx


def gradient_4point(f, dx):
    dfdx = np.zeros_like(f)
    dfdx[0] = (-3*f[0] + 4*f[1] - f[2])/(dx*2)
    dfdx[1] = (f[2] - f[0])/(dx*2)
    dfdx[-1] = (3*f[-1] - 4*f[-2] + f[-3])/(dx*2)
    dfdx[-2] = (f[-1] - f[-3])/(dx*2)
    for i in range(2, len(f)-2):
        dfdx[i] = (-f[i+2] + 8*f[i+1] - 8*f[i-1] + f[i-2])/(dx*12)

    return dfdx

