from geostrophicWind import *
import scipy.stats


def showErrorRelationship(endpoint=False, improved=False):
    N_track = 10
    N_steps = 10
    midpoint_errors = []
    N_values = []
    for s in range(N_steps):
        error_list = geostrophicWind(N=N_track, display_graphs=False, return_improved=improved)
        if not endpoint: midpoint_errors.append(abs(error_list[int(N_track / 2)]))
        if endpoint: midpoint_errors.append(abs(error_list[-1]))
        N_values.append(N_track)
        N_track += 10

    N_values = np.array(N_values)
    res = scipy.stats.linregress(np.log(N_values), np.log(midpoint_errors))
    res_values = np.exp(res.intercept + res.slope * np.log(N_values))

    plt.plot(N_values, midpoint_errors, 'k-', label="Error at midpoint")
    plt.legend (loc='best')
    plt.xlabel('Number of Intervals')
    plt.ylabel('Error')
    plt.savefig('geoErrorTest_reg'+str(int(improved))+'.png')
    plt.show()

    plt.loglog(N_values, midpoint_errors, 'k-', label="Error at midpoint")
    plt.loglog(N_values, res_values, 'r--', label="LinReg: Gradient =" + str(round(res.slope, 4)))
    plt.legend (loc='best')
    plt.xlabel('Number of Intervals')
    plt.ylabel('Error')
    plt.savefig('geoErrorTest'+str(int(improved))+'.png')
    plt.show()


showErrorRelationship(endpoint=True, improved=False)
